import { debug as createDebug } from 'debug';
import { Request, Response, Router } from 'express';
import fs from 'node:fs/promises';
import https from 'node:https';
import path from 'node:path';
import { Commit } from '../models/Commit';
import { Repo } from '../models/Repo';

const debug = createDebug('/repos');

export const repos = Router();

const LOCAL_REPOS_DATA = path.join(__dirname, '../../data/repos.json');
const GH_REPO_OWNER = 'silverorange';
const GH_REPO_URL = new URL('https://api.github.com');
const GH_HEADERS = {
  accept: 'application/vnd.github+json',
  'user-agent': 'SilverOrange-Foo',
};

repos.get('/', async (_: Request, res: Response) => {
  res.header('Cache-Control', 'no-store');

  res.status(200);

  const allRepos: Repo[] = [];

  for (let page = 1; true; page++) {
    const data = await fetchPaginatedRepos(page);
    if (data.length === 0) {
      const localRepos: Repo[] = JSON.parse(
        await fs
          .readFile(LOCAL_REPOS_DATA, {
            encoding: 'utf-8',
          })
          .catch((err) => {
            debug('Error reading file %s\n%O', LOCAL_REPOS_DATA, err);
            return '[]';
          })
      );
      allRepos.push(...localRepos);
      break;
    } else {
      allRepos.push(...data);
    }
  }

  // TODO: See README.md Task (A). Return repo data here. You’ve got this!
  res.json(allRepos.filter((repo) => !repo.fork));
});

repos.get('/:repoName/details', async (req: Request, res: Response) => {
  res.header('Cache-Control', 'no-store');

  res.status(200);

  const repoName = req.params.repoName;

  const [latestCommit, readme] = await Promise.all([
    fetchLastCommit(repoName),
    fetchRepoReadme(repoName),
  ]);

  const result: { commit?: Commit; readMeMd?: string } = {
    readMeMd: readme?.toString(),
  };

  if (latestCommit) {
    result.commit = latestCommit;
  }

  res.json(result);
});

/**
 * Fetch the contents of the README.md file of a given Repository
 * @param repoName The repository to get the README.md for
 * @returns A buffer containing the README.md file string
 */
const fetchRepoReadme = async (repoName: string) => {
  const FILE_NAME = `https://raw.githubusercontent.com/${GH_REPO_OWNER}/${repoName}/master/README.md`;
  return new Promise<Buffer>((resolve, reject) => {
    https
      .get(FILE_NAME, (fileRes) => {
        const data: Buffer[] = [];
        fileRes.on('data', (chunk) => data.push(Buffer.from(chunk, 'utf8')));
        fileRes.on('end', () => resolve(Buffer.concat(data)));
      })
      .on('error', reject);
  }).catch(() => Buffer.alloc(0));
};

/**
 * Fetches the last commit for the given repository
 * @param repoName The repo to fetch latest commit for
 */
const fetchLastCommit = async (repoName: string): Promise<Commit> => {
  // API Ref: https://docs.github.com/en/rest/commits/commits#list-commits
  const url = new URL(
    `repos/${GH_REPO_OWNER}/${repoName}/commits`,
    GH_REPO_URL
  );
  url.searchParams.set('per_page', '1');

  return new Promise<Buffer>((resolve, reject) => {
    https
      .get(
        url,
        {
          headers: GH_HEADERS,
        },
        (commitRes) => {
          const data: Buffer[] = [];
          commitRes.on('data', (chunk) =>
            data.push(Buffer.from(chunk, 'utf8'))
          );
          commitRes.on('end', () => resolve(Buffer.concat(data)));
        }
      )
      .on('error', reject);
  })
    .then((body: Buffer) => {
      const data = JSON.parse(body.toString());
      if (Array.isArray(data)) {
        return data?.[0];
      }
      return undefined;
    })
    .catch((err) => {
      debug('Error fetching commit for %s\n%O', repoName, err);
      return undefined;
    });
};

/**
 * Fetch all repositories at the given page
 * @param page The page to fetch
 * @returns A list of repositories
 */
const fetchPaginatedRepos = async (page: number = 1): Promise<Repo[]> => {
  // API Ref: https://docs.github.com/en/rest/repos/repos#list-repositories-for-a-user
  const url = new URL(`users/${GH_REPO_OWNER}/repos`, GH_REPO_URL);
  url.searchParams.set('page', `${page}`);
  url.searchParams.set('per_page', '100');

  return new Promise<Buffer>((resolve, reject) => {
    https
      .get(
        url,
        {
          headers: GH_HEADERS,
        },
        (reposRes) => {
          const data: Buffer[] = [];
          reposRes.on('data', (chunk) => data.push(Buffer.from(chunk, 'utf8')));
          reposRes.on('end', () => resolve(Buffer.concat(data)));
        }
      )
      .on('error', reject);
  }).then((body: Buffer) => {
    const data = JSON.parse(body.toString());

    if (Array.isArray(data)) {
      return data;
    }

    return [];
  });
};
