import { useQuery } from '@tanstack/react-query';
import axios from 'axios';
import RepoList from 'components/repository-list/RepoList';
import Spinner from 'components/Spinner';
import { Repository } from 'models/Repository';
import { ScrollRestoration } from 'react-router-dom';

const Home: React.FC = () => {
  const { data, refetch, isLoading, isRefetching, isError } = useQuery(
    ['reposData'],
    () =>
      axios
        .get<Repository[]>('/repos', {
          baseURL: process.env.REACT_APP_BACKEND_URL!,
          headers: {
            'Content-Type': 'application/json',
          },
          transformResponse: (dataResp) => {
            return JSON.parse(dataResp).map(
              // eslint-disable-next-line @typescript-eslint/naming-convention
              ({ id, forks_count, created_at, ...repo }: any) => ({
                ...repo,
                repoId: id,
                forksCount: forks_count,
                createdDate: new Date(created_at),
              })
            );
          },
        })
        .then(({ data: responseData }) => {
          return responseData.sort(
            (repoA, repoB) =>
              repoB.createdDate.getTime() - repoA.createdDate.getTime()
          );
        }),
    {
      staleTime: Infinity,
    }
  );

  return (
    <div className="container flex flex-col justify-center items-center flex-1 max-h-full mx-auto p-3">
      {isLoading || isRefetching ? (
        <div className="max-w-xs flex flex-col justify-center">
          <Spinner />
        </div>
      ) : (
        data?.length && <RepoList repos={data} />
      )}

      {!(isLoading || data?.length) && (
        <div className="max-w-md flex flex-col items-start">
          <h1 className="text-xl font-bold py-4">
            {isError
              ? 'Failed to fetch Repositories...'
              : 'No repositories found'}
          </h1>
          <button
            onClick={() => refetch()}
            disabled={isLoading || isRefetching}
            className="px-4 py-2 rounded-md self-center bg-red-500 font-semibold hover:bg-red-600 disabled:bg-gray-500 text-white"
          >
            Try again?
          </button>
        </div>
      )}
      <ScrollRestoration />
    </div>
  );
};

export default Home;
