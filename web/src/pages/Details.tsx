import { useQuery } from '@tanstack/react-query';
import axios from 'axios';
import RepoDetails from 'components/repository/RepoDetails';
import Spinner from 'components/Spinner';
import { RepositoryDetails } from 'models/RepositoryDetails';
import { useCallback } from 'react';
import {
  NavigationType,
  useNavigate,
  useNavigationType,
  useParams,
} from 'react-router-dom';

const Details: React.FC = () => {
  const { repoName } = useParams();
  const navigate = useNavigate();
  const navigationType = useNavigationType();

  const { data, isLoading, isFetching, refetch } = useQuery(
    ['repoDetails'],
    () =>
      axios
        .get<RepositoryDetails>(`/repos/${repoName?.toLowerCase()}/details`, {
          baseURL: process.env.REACT_APP_BACKEND_URL!,
          headers: {
            'Content-Type': 'application/json',
          },
          transformResponse: (detailsResp) => {
            const { commit, readMeMd } = JSON.parse(detailsResp);

            if (commit == null) {
              return null;
            }
            return {
              lastCommitDate: new Date(commit.commit.author.date),
              lastCommitAuthor: commit.commit.author.name,
              lastCommitComment: commit.commit.message,
              readMeContent: readMeMd,
            };
          },
        })
        .then(({ data: respData }) => respData)
  );

  const handleGoBack = useCallback(() => {
    navigationType === NavigationType.Pop ? navigate('/') : navigate(-1);
  }, [navigate, navigationType]);

  return (
    <div className="flex flex-col items-center p-3 h-full">
      <button
        onClick={handleGoBack}
        className="rounded-md px-3 py-2 my-4 bg-blue-100 self-start hover:bg-blue-400"
      >
        Back
      </button>
      {isLoading ||
        (isFetching && (
          <div className="w-full flex flex-1 justify-center items-center">
            <Spinner color="cyan" />
          </div>
        ))}
      {!isFetching && data && <RepoDetails {...data} />}

      {!isFetching && !data && (
        <div className="flex flex-col rounded-lg bg-gray-100 shadow-md p-3 max-w-md mx-auto">
          <p className="text-lg">
            No details found for <span className="font-bold">{repoName}</span>
          </p>
          <div className="grid grid-flow-col grid-cols-2 gap-x-4 py-2">
            <button
              onClick={() => {
                refetch();
              }}
              className="px-2 py-1 rounded-md text-green-400 font-semibold bg-green-100 hover:bg-green-200"
            >
              Try again
            </button>
            <button
              onClick={handleGoBack}
              className="px-2 py-1 rounded-md text-pink-500 font-semibold bg-pink-100 hover:bg-pink-200"
            >
              Go back
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Details;
