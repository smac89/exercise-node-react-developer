export interface RepositoryDetails {
  lastCommitDate: Date;
  lastCommitAuthor: string;
  lastCommitComment: string;
  readMeContent?: string;
}
