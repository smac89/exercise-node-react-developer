export interface Repository {
  name: string;
  description: string;
  language: string;
  forksCount: number;
  repoId: number;
  createdDate: Date;
}
