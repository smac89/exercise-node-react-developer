import DetailsPage from 'pages/Details';
import HomePage from 'pages/Home';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

const router = createBrowserRouter([
  {
    path: '/',
    element: <HomePage />,
  },
  {
    path: '/:repoName',
    element: <DetailsPage />,
  },
]);

export function App() {
  return (
    <div className="flex flex-col h-screen w-screen">
      <header className="flex text-center text-xl justify-center items-center p-2 text-gray-100 bg-gray-800 w-full">
        SilverOrange Repos
      </header>
      <main className="flex-1 min-h-0 w-full min-w-0">
        <RouterProvider router={router} />
      </main>
    </div>
  );
}
