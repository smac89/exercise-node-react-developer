import Repo from 'components/repository/Repo';
import { Repository as RepoModel } from 'models/Repository';
import { useEffect, useState } from 'react';

interface RepoListProps {
  repos: RepoModel[];
}

const RepoList: React.FC<RepoListProps> = ({ repos }) => {
  const [languages, setLanguages] = useState<string[]>([]);
  const [filterLanguage, setFilterLanguage] = useState<string>();

  useEffect(() => {
    const uniqueLanguages = new Set(
      repos.filter((repo) => repo.language).map((repo) => repo.language)
    );
    setLanguages(Array.from(uniqueLanguages));
  }, [repos]);

  return (
    <div className="flex flex-col items-center min-h-0 my-2 py-8 w-full max-w-3xl px-3">
      {filterLanguage && (
        <div className="py-2 text-center">
          <span>
            Filtered by Language:
            <span className="font-medium px-1">{filterLanguage}</span>
          </span>
          <button
            onClick={() => setFilterLanguage(undefined)}
            className="rounded-full text-white text-xs font-semibold font-mono h-4 w-4 bg-red-600 hover:bg-red-700"
          >
            x
          </button>
        </div>
      )}
      <div className="flex flex-wrap max-w-sm justify-around space-x-2 py-4">
        {languages.map((language) => (
          <button
            className="px-4 py-2 my-1 text-xs rounded-full bg-gray-700 text-gray-50 hover:bg-gray-900"
            key={language}
            onClick={() => setFilterLanguage(language)}
          >
            {language}
          </button>
        ))}
      </div>
      <div className="flex flex-col flex-1 overflow-auto max-w-5xl w-full min-w-0">
        {repos
          .filter((repo) => !filterLanguage || repo.language === filterLanguage)
          .map((repo) => (
            <div className="py-2 px-1 m-1" key={repo.repoId}>
              <Repo repo={repo} />
            </div>
          ))}
      </div>
    </div>
  );
};

export default RepoList;
