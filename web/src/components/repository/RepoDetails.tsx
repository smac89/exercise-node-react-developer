import { RepositoryDetails } from 'models/RepositoryDetails';
import React from 'react';

type RepoDetailsProp = RepositoryDetails;

const RepoDetails: React.FC<RepoDetailsProp> = ({
  lastCommitAuthor,
  lastCommitDate,
  lastCommitComment,
  readMeContent,
}) => {
  return (
    <div className="flex flex-col h-full min-h-0">
      <div className="flex flex-col max-w-sm space-y-3 pb-6 mx-auto text-sm">
        <div className="grid grid-cols-2">
          <span className="font-medium pr-2">Last Commit Date</span>
          <span className="font-light text-xs">
            {lastCommitDate.toString()}
          </span>
        </div>
        <div className="grid grid-cols-2">
          <span className="font-medium pr-2">Last Commit Author</span>
          <span className="font-light text-xs">{lastCommitAuthor}</span>
        </div>
        <div className="grid grid-cols-2">
          <span className="font-medium pr-2">Last Commit Comment</span>
          <span className="font-light text-xs">{lastCommitComment}</span>
        </div>
      </div>
      <div className="max-w-3xl mx-auto bg-gray-100 overflow-auto">
        <p>{readMeContent}</p>
      </div>
    </div>
  );
};

export default RepoDetails;
