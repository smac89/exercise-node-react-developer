import { Repository } from 'models/Repository';
import { Link } from 'react-router-dom';

interface RepoProps {
  repo: Repository;
}

const Repo: React.FC<RepoProps> = ({ repo }) => {
  return (
    <div className="flex flex-col p-4 shadow-md rounded-md w-full bg-gray-100">
      <div className="grid grid-flow-col grid-cols-2 pb-4">
        <h3 className="font-bold">
          <Link
            to={{ pathname: `/${repo.name}` }}
            className="visited:text-blue-400 text-blue-600 hover:underline"
          >
            {repo.name}
          </Link>
        </h3>
        <p className="flex flex-wrap font-light italic max-w-md">
          {repo.description ?? 'No Description'}
        </p>
      </div>
      <hr className="h-px" />
      <div className="grid grid-flow-col grid-cols-2">
        <p>
          <span className="text-sm font-bold pr-2 rtl:pl-2">Language:</span>
          <span className="text-xs font-light">
            {repo.language ?? 'Unknown'}
          </span>
        </p>
        <p>
          <span className="text-sm font-bold pr-2 rtl:pl-2">Forks Count:</span>
          <span className="text-xs font-light">{repo.forksCount}</span>
        </p>
      </div>
    </div>
  );
};

export default Repo;
